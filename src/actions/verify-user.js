import ResponseError from 'lib/response-error'

export default async ({ verificationToken, email, db }) => {
  let trx
  try {
    if (!verificationToken) {
      throw new ResponseError({
        code: 400,
        message: 'Verification Token Required'
      })
    }

    if (!email) {
      throw new ResponseError({ code: 400, message: 'Email Required' })
    }

    const user = await db('users').where({
      email: email,
      confirmation_token: verificationToken
    })

    if (!user || !user.length) {
      throw new ResponseError({ code: 400, message: 'Invalid Request' })
    }

    if (user[0].is_verified) {
      throw new ResponseError({
        code: 400,
        message: 'User/Email already verified'
      })
    }

    trx = await db.transaction()

    const updated = await trx('users')
      .update('is_verified', true)
      .where({ email: email, confirmation_token: verificationToken })
      .returning(['id'])

    return {
      verified: true,
      id: updated[0].id
    }
  } catch (error) {
    if (trx) {
      await trx.rollback()
    }
    throw error
  }
}
