import hash from 'lib/hash'
import emailService from 'lib/email'
import ResponseError from 'lib/response-error'

export default async ({
  email,
  username,
  password,
  passwordConfirmation,
  db
}) => {
  let trx

  try {
    if (!email) {
      throw new ResponseError({
        code: 400,
        message: 'Email cannot be left empty'
      })
    }

    if (!username) {
      throw new ResponseError({
        code: 400,
        message: 'Username cannot be left empty'
      })
    }

    if (!password || !password.length) {
      throw new ResponseError({
        code: 400,
        message: 'Password cannot be left empty'
      })
    }

    if (password !== passwordConfirmation) {
      throw new ResponseError({
        code: 400,
        message: 'The password does not match the confirmation'
      })
    }

    const userExists = await db('users').where({ email: email })

    if (userExists && userExists.length) {
      throw new ResponseError({
        code: 400,
        message: 'A User with this email already exists, try signing-in.'
      })
    }

    trx = await db.transaction()

    const confirmationToken = randomString({ length: 40 })

    const user = await trx('users')
      .insert({
        email: email,
        password: await hash.hash(password),
        confirmation_token: confirmationToken
      })
      .returning(['id'])

    const profile = await trx('profiles').insert({
      name: username,
      user_id: user[0].id
    })

    emailService.sendVerificationEmail(email, confirmationToken)

    return {
      id: user[0].id
    }
  } catch (err) {
    if (trx) {
      await trx.rollback()
    }
    throw err
  }
}
