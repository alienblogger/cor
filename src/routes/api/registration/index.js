import injectDatabase from 'lib/manage-db'
import ResponseError from 'lib/response-error'
import registerUser from 'actions/register-user'

const handler = async (req, res) => {
  if (req.method.toLowerCase() !== 'post') {
    throw new ResponseError({ code: 404, message: 'Not Found' })
  }

  const payload = req.body
  const response = await registerUser({
    db: req.db,
    username: payload.username,
    email: payload.email,
    password: payload.password,
    passwordConfirmation: payload.passwordConfirmation
  })

  return res.send({
    data: response
  })
}

export default injectDatabase(handler)
