import got from 'got'
import fs from 'fs'
import template from 'lodash.template'

export default {
  sendVerificationEmail (email, verificationToken) {
    const htmlFile = fs.readFileSync('templates/login-verification.html')
    const compiledTemplate = template(htmlFile)
    const mailerUrl = process.env.MAILER_URL

    const verificationLink =
      process.env.ORIGIN_URL +
      `/registration/confirm?token=${verificationToken}&email=${email}`

    got
      .post(mailerUrl, {
        json: {
          to: email,
          subject: `Login Verification - ${new Date().getTime()}`,
          html: compiledTemplate({
            verificationLink
          })
        }
      })
      .json()
      .catch((err) => err.response.json())
      .then(console.error)

    return true
  }
}
