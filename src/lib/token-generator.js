import jwt from 'jsonwebtoken'
import jwtSecret from 'configs/jwt'

export default (userDetails) => {
  const payload = {
    id: userDetails.id
  }

  return new Promise(function (resolve, reject) {
    jwt.sign(
      payload,
      Buffer.from(jwtSecret, 'base64'),
      { expiresIn: '1y' },
      function (err, token) {
        if (err) reject(err)
        return resolve(token)
      }
    )
  })
}
