import bcrypt from 'bcrypt'

export default {
  async hash (toHashString) {
    const salt = await bcrypt.genSalt()
    const hashed = await bcrypt.hash(toHashString, salt)
    return hashed
  },
  async verify (toCompareString, hashedString) {
    const valid = await bcrypt.compare(toCompareString, hashedString)
    return valid
  }
}
