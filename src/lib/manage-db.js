import knex from 'knex'
import ResponseError from 'lib/response-error'
import knexfile from '../../knexfile.js'

export default (handler) => {
  return async (req, res) => {
    try {
      const environment = process.env.NODE_ENV || 'development'
      const knexDefinitions = knexfile[environment]
      if (!knexDefinitions) {
        throw new ResponseError({ code: 500, message: 'DB Config missing' })
      }
      const db = knex(knexDefinitions)
      req.db = db
      await handler(req, res)
      await db.destroy()
    } catch (err) {
      if (err instanceof ResponseError) {
        res.status(err.code)
        res.send({
          error: err.message
        })
      } else {
        res.status(500)
        res.send({
          error: 'Oops! Something went wrong!',
          message: err.toString()
        })
      }
      throw err
    }
  }
}
