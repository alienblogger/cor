export default function ResponseError ({ code, message }) {
  this.message = new Error(message).toString()
  this.code = code
}
