import app from 'ftrouter'
import http from 'http'
import path from 'path'
import { existsSync } from 'fs'
import serveStatic from 'serve-static'
import runMiddleware from 'lib/run-middleware'

import { parse } from 'url'
import send from 'send'

const PORT = process.env.PORT || 3000
const APIREGEX = /^\/api.*/

require('dotenv').config()

app({
  basePath: path.join(__dirname, './routes')
}).then((appHandler) => {
  const httpServer = http.createServer(async (req, res) => {
    try {
      const fileDir = path.join(__dirname, 'client')

      var serve = serveStatic(fileDir, {
        index: ['index.html', 'index.htm']
      })

      const parsedUrl = parse(req.url, true)
      const { pathname } = parsedUrl
      await runMiddleware(req, res, serve)

      const fileExists = existsSync(path.join(fileDir, pathname))
      if (!APIREGEX.test(pathname)) {
        if (fileExists) {
          return send(req, pathname, { root: fileDir }).pipe(res)
        }
        return send(req, path.join(fileDir, 'index.html')).pipe(res)
      }

      appHandler(req, res)
    } catch (err) {
      console.error(err)
    }
  })

  httpServer.listen(PORT, () => {
    console.log('Listening on, ' + PORT)
  })
})
