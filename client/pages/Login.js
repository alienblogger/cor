import React from 'react'
import { useHistory } from 'react-router-dom'

export default function Home () {
  const router = useHistory()

  const handleLogin = () => {
    router.push('/home')
  }

  return (
    <>
      <r-grid columns='3'>
        <r-cell span='row'>
          <p align='center'>
            <input type='text' placeholder='email' />
          </p>
        </r-cell>
        <r-cell span='row'>
          <p align='center'>
            <input type='password' placeholder='password' />
          </p>
        </r-cell>
        <r-cell span='row'>
          <p align='center'>
            <button onClick={handleLogin}>Login</button>
          </p>
        </r-cell>
      </r-grid>
    </>
  )
}
