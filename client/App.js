import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Login from 'client/pages/Login'
import Home from 'client/pages/Home'

export default function App (props) {
  return (
    <>
      <div>
        <h1>COR</h1>
        <p align='center'>
          <small>Creator Objective Repository</small>
        </p>
        <main>
          <Router>
            <Switch>
              <Route path='/login'>
                <Login />
              </Route>
              <Route path='/home'>
                <Home />
              </Route>
            </Switch>
          </Router>
        </main>
      </div>
      <style jsx>
        {`
          h1 {
            font-size: 2.8rem;
            text-align: center;
          }
        `}
      </style>
    </>
  )
}
