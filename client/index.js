import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './styles/raster2.css'
import './styles/main.css'

function Index () {
  return (
    <>
      <App />
    </>
  )
}

ReactDOM.render(<Index />, document.getElementById('root'))
