exports.up = function (knex) {
  return knex.schema
    .createTable('users', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('email').unique().notNullable()
      table.string('password').notNullable()
      table.string('confirmation_token').nullable()
      table.boolean('is_verified').notNullable().default(false)
      table.boolean('is_active').notNullable().default(true)
      table.timestamps(true, true)
    })
    .createTable('profiles', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('name').notNullable()
      table.string('slug').notNullable()

      table.integer('user_id').unique().notNullable()
      table.boolean('is_active').notNullable().default(true)

      table.timestamps(true, true)

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('teams', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('name').notNullable()
      table.string('slug').notNullable()
      table.integer('owner_id').unique().notNullable()
      table.boolean('is_active').notNullable().default(true)

      table.timestamps(true, true)

      table
        .foreign('owner_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('team_user_mapping', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.enu('name', ['owner', 'member']).notNullable()
      table.integer('team_id').unique().notNullable()
      table.integer('user_id').unique().notNullable()
      table.boolean('is_active').notNullable().default(true)

      table.timestamps(true, true)

      table
        .foreign('team_id')
        .references('teams.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('plans', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('name').notNullable()
      table.integer('pricing').notNullable()
      table.boolean('is_active').notNullable().default(true)

      table.timestamps(true, true)
    })
    .createTable('billing', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('name').notNullable()
      table.integer('duration').nullable()
      table.date('expiry').nullable()
      table.boolean('is_active').notNullable().default(true)
      table.integer('plan_id').notNullable()
      table.integer('user_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('plan_id')
        .references('plans.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('payment_methods', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('customer_id').notNullable()
      table.string('customer_name').notNullable()
      table.string('card_type').notNullable()
      table.string('last4').notNullable()
      table.string('exp_month').notNullable()
      table.string('exp_year').notNullable()
      table.boolean('is_active').notNullable().default(true)
      table.integer('user_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('projects', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.string('name').notNullable()
      table.text('description').notNullable()
      table.boolean('is_active').notNullable().default(true)
      table.integer('user_id').nullable()
      table.integer('plan_id').notNullable()
      table.integer('team_id').nullable()

      table.timestamps(true, true)

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .foreign('plan_id')
        .references('plans.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .foreign('team_id')
        .references('teams.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('project_user_mapping', function (table) {
      table.increments('id').unique().primary().notNullable()

      table.integer('user_id').notNullable()
      table.integer('project_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('project_id')
        .references('projects.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('issues', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.text('name').notNullable()
      table.text('description').notNullable()
      table.enu('type', ['bug', 'feature', 'feature-request'])
      table.boolean('is_active').notNullable().default(true)
      table.integer('project_id').nullable()

      table.integer('user_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('project_id')
        .references('projects.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('comments', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.text('comment').notNullable()
      table.boolean('is_active').notNullable().default(true)
      table.integer('issue_id').nullable()
      table.integer('user_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('issue_id')
        .references('issues.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('issue_user_mapping', function (table) {
      table.increments('id').unique().primary().notNullable()
      table.integer('issue_id').nullable()
      table.integer('user_id').notNullable()

      table.timestamps(true, true)

      table
        .foreign('issue_id')
        .references('issues.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .createTable('tokens', function (table) {
      table.increments('id').primary().unique().notNullable()
      table.string('token_name').notNullable()
      table.string('token_secret').notNullable()
      table.text('token').unique().notNullable()
      table.boolean('is_verified').defaultTo('false').notNullable()
      table.string('email').notNullable()
      table.timestamps(true, true)
    })
}

exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('tokens')
    .dropTableIfExists('issue_user_mapping')
    .dropTableIfExists('comments')
    .dropTableIfExists('issues')
    .dropTableIfExists('project_user_mapping')
    .dropTableIfExists('projects')
    .dropTableIfExists('payment_methods')
    .dropTableIfExists('billing')
    .dropTableIfExists('plan_team_mapping')
    .dropTableIfExists('plans')
    .dropTableIfExists('team_user_mapping')
    .dropTableIfExists('teams')
    .dropTableIfExists('profiles')
    .dropTableIfExists('users')
}
