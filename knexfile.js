// Update with your config settings.

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      database: 'cor',
      user: 'admin',
      password: 'root'
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  production: {
    client: 'postgresql',
    connection: process.env.DB_URL,
    migrations: {
      tableName: 'knex_migrations'
    }
  }
}
