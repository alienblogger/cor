default:
	make clean
	make deps
	make package
	make tar

clean:
	rm -rf node_modules
	rm -rf dist 
	rm -rf .cache
	rm -rf packages

deps:
	rm -rf node_modules 
	npm --v
	npm i	

build:
	npm run compile
	npm run build:client

package:
	make build
	npm run pkg
tar:
	cd packages && tar -cvzf cor-macOS.tar.gz cor-macos
	cd packages && tar -cvzf cor-linux.tar.gz cor-linux
	cd packages && zip cor-windows.zip cor-win.exe
	find ./packages -type f -not -name '*.tar.gz' -and -not -name '*.zip' -delete

start:
	npm run start