exports.seed = function (knex) {
  return knex('plans')
    .del()
    .then(function () {
      return knex('plans').insert([
        { id: 1, pricing: 0, name: 'Hobby' },
        { id: 2, pricing: 5, name: 'Team' }
      ])
    })
}
